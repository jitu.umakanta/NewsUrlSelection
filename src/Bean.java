import java.util.List;

/**
 * Created by jitu on 12/3/2017.
 */
public class Bean {
    String url, mainblockTagName, mainblockAttributeName, mainblockAttributeValue, blockTagName, blockAttributeName, blockAttributeValue;
    int position;
    List tagName, attributeKey, attributeValue, extractKey, extractValue, name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMainblockTagName() {
        return mainblockTagName;
    }

    public void setMainblockTagName(String mainblockTagName) {
        this.mainblockTagName = mainblockTagName;
    }

    public String getMainblockAttributeName() {
        return mainblockAttributeName;
    }

    public void setMainblockAttributeName(String mainblockAttributeName) {
        this.mainblockAttributeName = mainblockAttributeName;
    }

    public String getMainblockAttributeValue() {
        return mainblockAttributeValue;
    }

    public void setMainblockAttributeValue(String mainblockAttributeValue) {
        this.mainblockAttributeValue = mainblockAttributeValue;
    }

    public String getBlockTagName() {
        return blockTagName;
    }

    public void setBlockTagName(String blockTagName) {
        this.blockTagName = blockTagName;
    }

    public String getBlockAttributeName() {
        return blockAttributeName;
    }

    public void setBlockAttributeName(String blockAttributeName) {
        this.blockAttributeName = blockAttributeName;
    }

    public String getBlockAttributeValue() {
        return blockAttributeValue;
    }

    public void setBlockAttributeValue(String blockAttributeValue) {
        this.blockAttributeValue = blockAttributeValue;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List getTagName() {
        return tagName;
    }

    public void setTagName(List tagName) {
        this.tagName = tagName;
    }

    public List getAttributeKey() {
        return attributeKey;
    }

    public void setAttributeKey(List attributeKey) {
        this.attributeKey = attributeKey;
    }

    public List getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(List attributeValue) {
        this.attributeValue = attributeValue;
    }

    public List getExtractKey() {
        return extractKey;
    }

    public void setExtractKey(List extractKey) {
        this.extractKey = extractKey;
    }

    public List getExtractValue() {
        return extractValue;
    }

    public void setExtractValue(List extractValue) {
        this.extractValue = extractValue;
    }

    public List getName() {
        return name;
    }

    public void setName(List name) {
        this.name = name;
    }
}
