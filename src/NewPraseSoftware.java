import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.*;

/**
 * Created by jitu on 12/5/2017.
 */
public class NewPraseSoftware {
    static String secondurl="http://bluedeserts.com/";
    static List<Map> all = new LinkedList();
    /**
     *
     * @param Url-main url of the website-String
     *
     * @param mainblockTagName-tag name of the main block-String
     * @param mainblockAttributeName-attribute name of the main block-String
     * @param mainblockAttributeValue-attribute value of the main block-String
     * @param position-position of the main block-String
     *
     * @param blockTagName-tag name of the card block-String
     * @param blockAttributeName-attribute name of the card block-String
     * @param blockAttributeValue-attribute value of the card block-String
     *
     * @param tagName-parent tag name of the required data-list
     * @param attributeKey-parent attribute name of the required data-list
     * @param attributeValue-parent attribute value of the required data-list
     *
     * @param extractKey-tag of required data-list
     * @param extractValue-attribute value of required data-list
     *
     * @param name-give "desc" for description -list
     *
     * @return list of map which contains the required data
     * @throws IOException
     */
    public static List<Map> moviesPrase(String Url, String mainblockTagName, String mainblockAttributeName, String mainblockAttributeValue, int position, String blockTagName, String blockAttributeName, String blockAttributeValue, List<String> tagName, List<String> attributeKey, List<String> attributeValue, List<String> extractKey, List<String> extractValue, List<String> name) throws IOException {

        all.clear();

        String newsPaperName = Url;
        int startIndex = newsPaperName.indexOf(".");
        int startIndex1 = newsPaperName.indexOf("/");
        int secondIndex = newsPaperName.indexOf(".", startIndex + 1);
        if (startIndex == 10 || startIndex == 11) {
            newsPaperName = newsPaperName.substring(startIndex + 1, secondIndex);
        } else {
            newsPaperName = newsPaperName.substring(startIndex1 + 2, startIndex);
        }
        System.out.println("newsPaperName: " + newsPaperName);


        Document document = null;


        try {
            document = Jsoup.connect(Url).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
            Element bodyElement = document.select("body").get(0);

            if (mainblockTagName.length() != 0) {
                Element mainBlock = bodyElement.getElementsByAttributeValue(mainblockAttributeName, mainblockAttributeValue).tagName(mainblockTagName).get(position);

                gettingNoOfBlockElement(mainBlock, blockTagName, blockAttributeName, blockAttributeValue, tagName, attributeKey, attributeValue, extractKey, extractValue, name);
            } else {

                gettingNoOfBlockElement(bodyElement, blockTagName, blockAttributeName, blockAttributeValue, tagName, attributeKey, attributeValue, extractKey, extractValue, name);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return all;
    }

    public static void gettingNoOfBlockElement(Element element, String blockTagName, String blockAttributeName, String blockAttributeValue, List<String> tagName, List<String> attributeKey, List<String> attributeValue, List<String> extractKey, List<String> extractValue, List<String> name) {

        //System.out.println("blockTagNamelength:: "+blockTagName.length() +" blockAttributeNamelength::  "+ blockAttributeName.length() != 0 +" blockAttributeValue::  "+ blockAttributeValue.length());

        //all are present
        if (blockTagName.length() != 0 && blockAttributeName.length() != 0 && blockAttributeValue.length() != 0) {

            //System.out.println("Total no of blocks: " + element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).tagName(blockTagName).size());
            for (int i = 0; i < element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).tagName(blockTagName).size(); i++) {
                Element blockElement = element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).tagName(blockTagName).get(i);

                //System.out.println(blockElement);
                //System.out.println("block: " + i);
                praseinsideBlockElement(blockElement, tagName, attributeKey, attributeValue, extractKey, extractValue, name);

            }

        } else

            //attribute key and attribute value
            if (blockTagName.length() == 0 && blockAttributeName.length() != 0 && blockAttributeValue.length() != 0) {

                //System.out.println("Total no of blocks: " + element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).size());
                for (int i = 0; i < element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).size(); i++) {
                    Element blockElement = element.getElementsByAttributeValue(blockAttributeName, blockAttributeValue).get(i);

                    //System.out.println(blockElement);
                    //System.out.println("block: " + i);
                    praseinsideBlockElement(blockElement, tagName, attributeKey, attributeValue, extractKey, extractValue, name);

                }

            } else
                //only tagname and attribute name
                if (blockTagName.length() != 0 && blockAttributeName.length() != 0 && blockAttributeValue.length() == 0) {

                    // System.out.println("Total no of blocks: " + element.getElementsByAttribute(blockAttributeName).tagName(blockTagName).size());
                    for (int i = 0; i < element.getElementsByAttribute(blockAttributeName).tagName(blockTagName).size(); i++) {
                        Element blockElement = element.getElementsByAttribute(blockAttributeName).tagName(blockTagName).get(i);

                        //System.out.println(blockElement);
                        // System.out.println("block: " + i);
                        praseinsideBlockElement(blockElement, tagName, attributeKey, attributeValue, extractKey, extractValue, name);

                    }


                } else
                    //only attributename
                    if (blockTagName.length() == 0 && blockAttributeName.length() != 0 && blockAttributeValue.length() == 0) {

                        //System.out.println("Total no of blocks: " + element.getElementsByAttribute(blockAttributeName).size());
                        for (int i = 0; i < element.getElementsByAttribute(blockAttributeName).size(); i++) {
                            Element blockElement = element.getElementsByAttribute(blockAttributeName).get(i);

                            //System.out.println(blockElement);
                            //System.out.println("block: " + i);
                            praseinsideBlockElement(blockElement, tagName, attributeKey, attributeValue, extractKey, extractValue, name);

                        }


                    } else
                        //only tag name
                        if (blockTagName.length() != 0 && blockAttributeName.length() == 0 && blockAttributeValue.length() == 0) {

                            //System.out.println("Total no of blocks: " + element.getElementsByTag(blockTagName).size());
                            for (int i = 0; i < element.getElementsByTag(blockTagName).size(); i++) {
                                Element blockElement = element.getElementsByTag(blockTagName).get(i);

                                //System.out.println(blockElement);
                                //System.out.println("block: " + i);
                                praseinsideBlockElement(blockElement, tagName, attributeKey, attributeValue, extractKey, extractValue, name);

                            }

                        }


    }

    static Map<String, String> b;
    public static void praseinsideBlockElement(Element blockElement, List<String> tagName, List<String> attributeKey, List<String> attributeValue, List<String> extractKey, List<String> extractValue, List<String> name) {

        // System.out.println(blockElement);

        b = new LinkedHashMap<>();
        //iteratng the list
        for (int q = 0; q < tagName.size(); q++) {

            int totalInsideBlockElement = 0;
            String data;
            //System.out.println("tag length: " + tagName.get(q).length() + " key length: " + attributeKey.get(q).length() + " value length: " + attributeValue.get(q).length());

            if (tagName.get(q).length() != 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() != 0) {
                totalInsideBlockElement = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    //String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    //System.out.println(name.get(q) + ": " + data);
                    b.put(name.get(q), data);

                }


            } else if (tagName.get(q).length() == 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() != 0) {
                totalInsideBlockElement = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    //System.out.println(name.get(q) + ": " + data);
                    b.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() != 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() == 0) {
                totalInsideBlockElement = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    //System.out.println(name.get(q) + ": " + data);
                    b.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() == 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() == 0) {
                totalInsideBlockElement = blockElement.getElementsByAttribute(attributeKey.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttribute(attributeKey.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttribute(attributeKey.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }

                    b.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() != 0 && attributeKey.get(q).length() == 0 && attributeValue.get(q).length() == 0) {
                // System.out.println(blockElement);
                // System.out.println("---------------------");
                int totalInsideBlockElement1 = blockElement.getElementsByTag(tagName.get(q)).size();
                // System.out.println("totalInsideBlockElement1: " + totalInsideBlockElement1);
                //String data = null;
                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement1; k++) {

                    Element f = blockElement.getElementsByTag(tagName.get(q)).get(k);
                    // System.out.println(f);
                    data = f.select(extractKey.get(q)).attr("abs:" + extractValue.get(q));
                    //System.out.println(data);
                    b.put(name.get(q) + " " + k, data);
                }

                //System.out.println();
            }
//----------------------------------------------------------------------------------------------------------------------
            //getting the all information from the block
            //System.out.println("link:: " + b.get("link") + " image:: " + b.get("image"));
            //if u want to go further extract from single url


            System.out.println("b.get: "+b.get("link"));
            if(b.get("link")==null){

                secondurl="http://bluedeserts.com/";
            }else{
                secondurl=b.get("link");
            }

                Document document = null;
                try {
                    document = Jsoup.connect(secondurl).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Element bodyElement1 = document.select("body").get(0);

                List<String> tagName5 = new ArrayList();
                List<String> attributeKey5 = new ArrayList();
                List<String> attributeValue5 = new ArrayList();
                List<String> extractKey5 = new ArrayList();
                List<String> extractValue5 = new ArrayList();
                List<String> name5 = new ArrayList();



                tagName5.add("div");
                attributeKey5.add("class");
                attributeValue5.add("demo-gallery");
                extractKey5.add("img");
                extractValue5.add("src");
                name5.add("image1");

            tagName5.add("h1");
            attributeKey5.add("class");
            attributeValue5.add("font-title");
            extractKey5.add("");
            extractValue5.add("");
            name5.add("desc");

               Map<String,String> v= praseinsideBlockElement1(bodyElement1, tagName5, attributeKey5, attributeValue5, extractKey5, extractValue5, name5);
          /*

            for (Map.Entry<String, String> entry : v.entrySet())
            {
               // System.out.println(entry.getKey() + "/" + entry.getValue());
                b.put(entry.getKey() , entry.getValue());
            }*/

           b.put("image1",v.get("image1"));
            b.put("descriptiondetails",v.get("desc"));

            b.put("aaa" , "lll");

        }
        all.add(b);
    }
    static Map<String, String> b1;
    public static Map<String, String> praseinsideBlockElement1(Element blockElement, List<String> tagName, List<String> attributeKey, List<String> attributeValue, List<String> extractKey, List<String> extractValue, List<String> name) {

        b1 = new LinkedHashMap<>();
        for (int q = 0; q < tagName.size(); q++) {

            int totalInsideBlockElement = 0;
            String data;
            if (tagName.get(q).length() != 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() != 0) {
                totalInsideBlockElement = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    //String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).tagName(tagName.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    b1.put(name.get(q), data);

                }


            } else if (tagName.get(q).length() == 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() != 0) {
                totalInsideBlockElement = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttributeValue(attributeKey.get(q), attributeValue.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    //System.out.println(name.get(q) + ": " + data);
                    b1.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() != 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() == 0) {
                totalInsideBlockElement = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttribute(attributeKey.get(q)).tagName(tagName.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }
                    //System.out.println(name.get(q) + ": " + data);
                    b1.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() == 0 && attributeKey.get(q).length() != 0 && attributeValue.get(q).length() == 0) {
                totalInsideBlockElement = blockElement.getElementsByAttribute(attributeKey.get(q)).size();

                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement; k++) {

                    // String data;
                    if (name.get(q).equals("desc")) {

                        if (attributeKey.get(q).length() == 0 || attributeValue.get(q).length() == 0) {
                            data = blockElement.tagName(tagName.get(q)).text();
                        } else {
                            data = blockElement.getElementsByAttribute(attributeKey.get(q)).get(k).text();
                        }
                    } else {


                        data = blockElement.getElementsByAttribute(attributeKey.get(q)).get(k).select(extractKey.get(q)).attr("abs:" + extractValue.get(q));


                    }

                    b1.put(name.get(q), data);
                }


            } else if (tagName.get(q).length() != 0 && attributeKey.get(q).length() == 0 && attributeValue.get(q).length() == 0) {
                // System.out.println(blockElement);
                // System.out.println("---------------------");
                int totalInsideBlockElement1 = blockElement.getElementsByTag(tagName.get(q)).size();
                // System.out.println("totalInsideBlockElement1: " + totalInsideBlockElement1);
                //String data = null;
                //total no of parents of the that item
                for (int k = 0; k < totalInsideBlockElement1; k++) {

                    Element f = blockElement.getElementsByTag(tagName.get(q)).get(k);
                    // System.out.println(f);
                    data = f.select(extractKey.get(q)).attr("abs:" + extractValue.get(q));
                    //System.out.println(data);
                    b1.put(name.get(q) + " " + k, data);
                }

                //System.out.println();
            }

            //getting the all information from the single page
            // System.out.println("link:: " + b.get("link") + " image:: " + b.get("image"));
            //go to server

        }

        return b1;

    }

}
