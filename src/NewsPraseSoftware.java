
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * Created by jitu on 11/19/2017.
 */
public class NewsPraseSoftware {


    public static void newsPrase(String Url, String linkAttributeKey, String linkAttrbuteValue, String headingAttrtbuteKey, String headingAttrtbuteValue, String imageAttrtbuteKey, String imageAttrtbuteValue, String descriptionAttrtbuteKey, String descriptionAttrtbuteValue, String removePartFromDescription) throws IOException {

        String newsPaperName = Url;
        int startIndex = newsPaperName.indexOf(".");
        int startIndex1 = newsPaperName.indexOf("/");
        int secondIndex = newsPaperName.indexOf(".", startIndex + 1);
        if (startIndex == 10 || startIndex == 11) {
            newsPaperName = newsPaperName.substring(startIndex + 1, secondIndex);
        } else {
            newsPaperName = newsPaperName.substring(startIndex1 + 2, startIndex);
        }
        System.out.println("newsPaperName: " + newsPaperName);

        Document document = null;
        try {
            document = Jsoup.connect(Url).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
            Element bodyElement = document.select("body").get(0);

            // System.out.println(bodyElement);


             //getting all links from a url
            for (int i = 0; i < bodyElement.getElementsByAttributeValueContaining(linkAttributeKey, linkAttrbuteValue).size(); i++) {

                String websiteName = newsPaperName;
                String singlepagelink = "nolink";
                String heading = "noheading";
                String imagelink = "noimagelink";
                String description = "nodescription";


                try {
                    Element element = bodyElement.getElementsByAttributeValueContaining(linkAttributeKey, linkAttrbuteValue).get(i);
                    // System.out.println(element);
                    singlepagelink = element.select("a").attr("abs:href");
                    System.out.println("link: " + singlepagelink);
                } catch (Exception e) {
                    System.out.println("no link");
                }
                Document singlepage = null;
                try {
                    singlepage = Jsoup.connect(singlepagelink).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
                } catch (Exception e) {
                    System.out.println("document prasing error");
                }

                try {
                    Element headingElement = singlepage.getElementsByAttributeValueContaining(headingAttrtbuteKey, headingAttrtbuteValue).get(0);
                    heading = headingElement.text();
                    if (heading.length() == 0) {
                        heading = headingElement.attr("content");
                    }

                    System.out.println("heading: " + heading);
                } catch (Exception e) {
                    System.out.println("no heading");
                }
                try {
                    Element element2 = singlepage.getElementsByAttributeValueContaining(imageAttrtbuteKey, imageAttrtbuteValue).get(0);
                    imagelink = element2.select("img").attr("abs:src");
                    System.out.println("image: " + imagelink);
                } catch (Exception e) {
                    System.out.println("no image");
                }

                try {
                    String descriptionvalue = singlepage.getElementsByAttributeValueContaining(descriptionAttrtbuteKey, descriptionAttrtbuteValue).text();

                    String description1 = descriptionvalue.toString().trim();
                    String description2 = description1.replace("\"", "-");
                    String description3 = description2.replace("'", "");
                    String description4 = description3;
                    description4 = description4.substring(description4.indexOf(".") + 1, description4.lastIndexOf("."));
                    description = description4.replace(removePartFromDescription, "");
                    System.out.println(description);
                } catch (Exception e) {
                    System.out.println("no description");
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
